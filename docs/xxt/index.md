---
title: "超星学习通小助手"
slug: Xxt
date: 2022-05-07T14:11:17+08:00
draft: false
---

## 简介

[超星学习通小助手[章节、作业、考试] 优化课程切换|支持新版](https://greasyfork.org/zh-CN/scripts/435357-%E8%B6%85%E6%98%9F%E5%AD%A6%E4%B9%A0%E9%80%9A%E5%B0%8F%E5%8A%A9%E6%89%8B-%E7%AB%A0%E8%8A%82-%E4%BD%9C%E4%B8%9A-%E8%80%83%E8%AF%95-%E4%BC%98%E5%8C%96%E8%AF%BE%E7%A8%8B%E5%88%87%E6%8D%A2-%E6%94%AF%E6%8C%81%E6%96%B0%E7%89%88)

一个简短的介绍

## 使用教程

一些个比较详细的教程

## 使用体验

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

