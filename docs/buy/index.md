---
title: "超简洁的网购省钱小助手"
slug: gfbuy
date: 2022-05-07T14:14:07+08:00
draft: false
---

## 简介

[! 超简洁的网购省钱小助手，自动显示京东、淘宝、聚划算、天猫隐藏优惠券与历史价格。简洁无广告，一目了然，让您告别虚假降价。持续维护中...](https://greasyfork.org/zh-CN/scripts/418355-%E8%B6%85%E7%AE%80%E6%B4%81%E7%9A%84%E7%BD%91%E8%B4%AD%E7%9C%81%E9%92%B1%E5%B0%8F%E5%8A%A9%E6%89%8B-%E8%87%AA%E5%8A%A8%E6%98%BE%E7%A4%BA%E4%BA%AC%E4%B8%9C-%E6%B7%98%E5%AE%9D-%E8%81%9A%E5%88%92%E7%AE%97-%E5%A4%A9%E7%8C%AB%E9%9A%90%E8%97%8F%E4%BC%98%E6%83%A0%E5%88%B8%E4%B8%8E%E5%8E%86%E5%8F%B2%E4%BB%B7%E6%A0%BC-%E7%AE%80%E6%B4%81%E6%97%A0%E5%B9%BF%E5%91%8A-%E4%B8%80%E7%9B%AE%E4%BA%86%E7%84%B6-%E8%AE%A9%E6%82%A8%E5%91%8A%E5%88%AB%E8%99%9A%E5%81%87%E9%99%8D%E4%BB%B7-%E6%8C%81%E7%BB%AD%E7%BB%B4%E6%8A%A4%E4%B8%AD)

一个简短的介绍

## 使用教程

一些个比较详细的教程

## 小编吐槽

@stark

stark 的想法

@royse

royse 的想法

## 附录

### Reference

参考的文章 1

### 反馈

如果您有好的工具软件/网站可以推荐或是有需要解决的小问题，可以[联系我们](help@1kbtool.com)。

### 版权声明

本网站所收集的所有资源均来自互联网。如有侵权，请发邮件至 support@1kbtool.com。

原载于 [1kbTool](https://1kbtool.com/) 转载请联系[作者](support@1kbtool.com)获得授权。

